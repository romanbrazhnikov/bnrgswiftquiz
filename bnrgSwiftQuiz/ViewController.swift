//
//  ViewController.swift
//  bnrgSwiftQuiz
//
//  Created by Roman Brazhnikov on 22/03/2019.
//  Copyright © 2019 Roman Brazhnikov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var questionLabel: UILabel!
    @IBOutlet var answerLabel: UILabel!
    
    let questions = [
        "What is the capital of Russia?",
        "What is cognac made of?",
        "What is 2 * 2?"
    ]
    
    let answers = [
        "Moscow",
        "Grapes",
        "4"
    ]
    
    var currentQuestionIndex = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        questionLabel.text = questions[currentQuestionIndex]
    }

    @IBAction func showNextQuestion(_ sender: UIButton) {
        currentQuestionIndex += 1
        
        if currentQuestionIndex == questions.count {
            currentQuestionIndex = 0
        }
        
        questionLabel.text = questions[currentQuestionIndex]
        
        answerLabel.text = "???"
    }
 
    @IBAction func showAnswer(_ sender: UIButton) {
        answerLabel.text = answers[currentQuestionIndex]
    }
    
}

